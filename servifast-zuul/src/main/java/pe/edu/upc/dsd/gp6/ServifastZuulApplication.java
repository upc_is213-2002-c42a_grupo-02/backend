package pe.edu.upc.dsd.gp6;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@SpringBootApplication
@EnableEurekaClient // Actúa como un cliente eureka
@EnableZuulProxy // Habilitar Zuul
public class ServifastZuulApplication {

    public static void main(String[] args) {
	SpringApplication.run(ServifastZuulApplication.class, args);
    }

}
