package pe.edu.upc.dsd.gp6.jms;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import pe.edu.upc.dsd.gp6.model.User;

@Component
public class JmsMailListener {

    private Boolean sendMail(InternetAddress[] sendTo, InternetAddress[] sendCC, String subject, String contentHtml) {
	Session session = Session.getInstance(JmsMethods.configEmailProperties(), new javax.mail.Authenticator() {
	    protected PasswordAuthentication getPasswordAuthentication() {
		return new PasswordAuthentication(JmsMethods.EMAIL_TO_NOTIFY, JmsMethods.PASSWORDD_EMAIL_TO_NOTIFY_);
	    }
	});
	try {
	    Message message = new MimeMessage(session);
	    message.setFrom(new InternetAddress(JmsMethods.EMAIL_TO_NOTIFY));
	    if (sendTo != null)
		message.setRecipients(Message.RecipientType.TO, sendTo);
	    if (sendCC != null)
		message.setRecipients(Message.RecipientType.TO, sendCC);
	    message.setSubject(subject);
	    message.setContent(contentHtml, "text/html");
	    Transport.send(message);
	} catch (MessagingException e) {
	    return false;
	}
	return true;
    }

    @Value("classpath:mailBienvenida.html")
    Resource resourceFile;

    @JmsListener(destination = "jms.servifast")
    public void miMensaje(String mensajeJson) {
	System.out.println("Recibido:" + mensajeJson);
	ObjectMapper mapper = new ObjectMapper();
	try {
	    JsonNode jsonNode = mapper.readTree(mensajeJson);

	    String email = jsonNode.get("email").asText();
	    String username = jsonNode.get("username").asText();

	    InternetAddress[] sendTo = new InternetAddress[1];
	    sendTo[0] = new InternetAddress(email);
	    String subject = "Bienvenido a ServiFast";

	    String contentHtml = IOUtils.toString(resourceFile.getInputStream());
	    contentHtml = contentHtml.replace("Usuario: xxxxxxx", "Usuario: " + username);
	    contentHtml = contentHtml.replace("xxxx@xxxxxxxxxx", email);

	    sendMail(sendTo, null, subject, contentHtml);

	} catch (Exception e) {
	    // TODO Auto-generated catch block
	    System.out.println(e.getMessage());
	    System.out.println("No se pudo registrar");
	}
    }
}
