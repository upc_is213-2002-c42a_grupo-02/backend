package pe.edu.upc.dsd.gp6.model;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {

    private String username;
    private String email;
    private String name;
    private String lastname;
    private Date datebirth;

    public String getUsername() {
	return username;
    }

    public void setUsername(String username) {
	this.username = username;
    }

    public String getEmail() {
	return email;
    }

    public void setEmail(String email) {
	this.email = email;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public String getLastname() {
	return lastname;
    }

    public void setLastname(String lastname) {
	this.lastname = lastname;
    }

    public Date getDatebirth() {
	return datebirth;
    }

    public void setDatebirth(Date datebirth) {
	this.datebirth = datebirth;
    }

}
