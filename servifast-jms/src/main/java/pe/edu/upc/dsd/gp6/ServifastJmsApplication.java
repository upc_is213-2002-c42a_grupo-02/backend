package pe.edu.upc.dsd.gp6;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServifastJmsApplication {

    public static void main(String[] args) {
	SpringApplication.run(ServifastJmsApplication.class, args);
    }

}
