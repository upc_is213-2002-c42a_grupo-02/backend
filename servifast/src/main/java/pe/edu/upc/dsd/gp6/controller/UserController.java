package pe.edu.upc.dsd.gp6.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import pe.edu.upc.dsd.gp6.model.User;
import pe.edu.upc.dsd.gp6.service.UserService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/")
public class UserController {

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    UserService usuarioService;

    @Autowired
    private Environment env;

    @GetMapping("/admin")
    public String homeAdmin() {
	return "Este es administrador de area de Galeria service corriendo en el puerto: "
		+ env.getProperty("local.server.port");
    }

    @PostMapping("/user")
    public User registrar(@RequestBody User usuario) {
	User p;
	try {
	    p = usuarioService.registrar(usuario);
	} catch (Exception e) {
	    throw new ResponseStatusException(HttpStatus.NOT_FOUND, "El usuario no se pudo crear", e);
	}
	return p;
    }

    @PutMapping("/user")
    public User actualizar(@RequestBody User usuario) {
	User p;
	try {
	    p = usuarioService.registrar(usuario);
	} catch (Exception e) {
	    throw new ResponseStatusException(HttpStatus.NOT_FOUND, "El usuario no se pudo crear", e);
	}
	return p;
    }

    @GetMapping("/users")
    public List<User> list() {
	List<User> p;
	try {
	    p = restTemplate.getForObject("http://resource-service/users", List.class);
	} catch (Exception e) {
	    throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No se pudo obtener información de usuarios", e);
	}
	return p;
    }

    @PostMapping("/user/login")
    public User login(@RequestBody User usuario) {
	User p;
	try {
	    p = usuarioService.login(usuario);
	} catch (Exception e) {
	    throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Error de autentificación", e);
	}
	return p;
    }

    @GetMapping("/user/{id}")
    public User login(@PathVariable("id") Long id) {
	User p;
	try {
	    Map<String, String> uriParams = new HashMap<String, String>();
	    uriParams.put("id", id.toString());
	    p = restTemplate.getForObject("http://resource-service/user/find/{id}", User.class, uriParams);
	} catch (Exception e) {
	    throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Error al obtener usuario", e);
	}
	return p;
    }

    @GetMapping("/user/find/{username}")
    public User login(@PathVariable("username") String username) {
	User p;
	try {
	    Map<String, String> uriParams = new HashMap<String, String>();
	    uriParams.put("username", username);
	    p = restTemplate.getForObject("http://resource-service/user/find/{username}", User.class, uriParams);
	} catch (Exception e) {
	    throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Error al obtener usuario", e);
	}
	return p;
    }

}
