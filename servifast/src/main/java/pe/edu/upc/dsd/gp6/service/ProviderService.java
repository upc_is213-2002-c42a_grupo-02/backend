package pe.edu.upc.dsd.gp6.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import pe.edu.upc.dsd.gp6.dao.ProviderDao;
import pe.edu.upc.dsd.gp6.model.Provider;
import pe.edu.upc.dsd.gp6.producer.JmsProducer;

@Service
public class ProviderService {

    @Autowired
    ProviderDao providerDao;

    @Autowired
    JmsProducer jmsservifast;

    @Transactional
    public Provider registrar(Provider provider) {
	ObjectMapper mapper = new ObjectMapper();
	Provider u = providerDao.save(provider);
	try {
	    jmsservifast.send(mapper.writeValueAsString(u));
	} catch (JsonProcessingException e) {
	    e.printStackTrace();
	}
	return u;
    }

    public List<Provider> list() {
	return (List<Provider>) providerDao.findAll();
    }

    public Provider login(Provider provider) throws Exception {
	Provider p = providerDao.findByUsername(provider.getUsername())
		.orElseThrow(() -> new Exception("Proveedor no existe"));
	if (p != null && p.getSecret().equals(provider.getSecret())) {
	    return p;
	} else
	    throw new Exception("Contraseña incorrecta");
    }

    public Provider findByUsername(String username) throws Exception {
	Provider p = providerDao.findByUsername(username).orElseThrow(() -> new Exception("Proveedor no existe"));
	if (p != null) {
	    return p;
	} else
	    throw new Exception("Proveedor no existe");
    }

    public Provider findById(Long id) throws Exception {
	Provider p = providerDao.findById(id).orElseThrow(() -> new Exception("Proveedor no existe"));
	if (p != null) {
	    return p;
	} else
	    throw new Exception("Proveedor no existe");
    }

}
