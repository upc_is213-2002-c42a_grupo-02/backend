package pe.edu.upc.dsd.gp6.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class UserAddress implements Serializable {

    @Column(name = "address", length = 100)
    private String address;

    @Override
    public String toString() {
	return "UserAddress [address=" + address + "]";
    }

    public String getAddress() {
	return address;
    }

    public void setAddress(String address) {
	this.address = address;
    }

}
