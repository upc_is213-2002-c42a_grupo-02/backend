package pe.edu.upc.dsd.gp6.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import pe.edu.upc.dsd.gp6.model.Request;
import pe.edu.upc.dsd.gp6.service.RequestService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/")
public class RequestController {

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    RequestService requestService;

    @PostMapping("/request")
    public Request actualizar(@RequestBody Request request) {
	Request p;
	try {
	    p = requestService.save(request);
	} catch (Exception e) {
	    throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
	}
	return p;
    }

    @GetMapping("/requests/user/{username}")
    public List<Request> lisBytUser(@PathVariable("username") String username) {
	List<Request> p;
	try {
	    Map<String, String> uriParams = new HashMap<String, String>();
	    uriParams.put("username", username);
	    p = restTemplate.getForObject("http://resource-service/requests/user/{username}", List.class,
		    uriParams);
	} catch (Exception e) {
	    throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
	}
	return p;
    }

    @GetMapping("/requests/provider/{username}")
    public List<Request> listByProvider(@PathVariable("username") String username) {
	List<Request> p;
	try {
	    Map<String, String> uriParams = new HashMap<String, String>();
	    uriParams.put("username", username);
	    p = restTemplate.getForObject("http://resource-service/requests/provider/{username}", List.class,
		    uriParams);
	} catch (Exception e) {
	    throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
	}
	return p;
    }

}
