package pe.edu.upc.dsd.gp6.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import pe.edu.upc.dsd.gp6.model.Category;
import pe.edu.upc.dsd.gp6.service.CategoryService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/")
public class CategoryController {

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    CategoryService categoryService;

    @PostMapping("/category")
    public Category registrar(@RequestBody Category category) {
	Category p;
	try {
	    p = categoryService.registrar(category);
	} catch (Exception e) {
	    throw new ResponseStatusException(HttpStatus.NOT_FOUND, "La categoria no se pudo crear", e);
	}
	return p;
    }

    @PutMapping("/category")
    public Category actualizar(@RequestBody Category category) {
	Category p;
	try {
	    p = categoryService.registrar(category);
	} catch (Exception e) {
	    throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Error al actualizar categoria", e);
	}
	return p;
    }

    @GetMapping("/categories")
    public List<Category> list() {
	List<Category> p;
	try {
	    p = restTemplate.getForObject("http://resource-service/categories", List.class);
	} catch (Exception e) {
	    throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No existe informacion de categorias", e);
	}
	return p;
    }

}
