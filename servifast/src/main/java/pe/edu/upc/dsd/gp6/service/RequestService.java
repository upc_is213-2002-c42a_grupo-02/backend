package pe.edu.upc.dsd.gp6.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.edu.upc.dsd.gp6.dao.ProviderDao;
import pe.edu.upc.dsd.gp6.dao.RequestDao;
import pe.edu.upc.dsd.gp6.dao.UserDao;
import pe.edu.upc.dsd.gp6.model.Provider;
import pe.edu.upc.dsd.gp6.model.Request;
import pe.edu.upc.dsd.gp6.model.User;

@Service
public class RequestService {

    @Autowired
    RequestDao requestDao;

    @Autowired
    UserDao userDao;

    @Autowired
    ProviderDao providerDao;

    public Request save(Request request) throws Exception {
	User user = userDao.findByUsername(request.getUser().getUsername())
		.orElseThrow(() -> new Exception("El usuario no existe"));
	Provider provider = providerDao.findByUsername(request.getProvider().getUsername())
		.orElseThrow(() -> new Exception("El proveedor no existe"));
	request.setUser(user);
	request.setProvider(provider);
	request.setDate(new Date());
	return requestDao.save(request);
    }

    public List<Request> findByUser(String username) {
	return requestDao.findByUser(username);
    }

    public List<Request> findByProvider(String username) {
	return requestDao.findByProvider(username);
    }

}
