package pe.edu.upc.dsd.gp6.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import pe.edu.upc.dsd.gp6.dao.UserDao;
import pe.edu.upc.dsd.gp6.model.User;
import pe.edu.upc.dsd.gp6.producer.JmsProducer;

@Service
public class UserService {

    @Autowired
    UserDao usuarioDao;

    @Autowired
    JmsProducer jmsservifast;

    @Transactional
    public User registrar(User user) {
	ObjectMapper mapper = new ObjectMapper();
	User u = usuarioDao.save(user);
	try {
	    jmsservifast.send(mapper.writeValueAsString(u));
	} catch (JsonProcessingException e) {
	    e.printStackTrace();
	}
	return u;
    }

    public List<User> list() {
	return (List<User>) usuarioDao.findAll();
    }

    public User login(User user) throws Exception {
	User u = usuarioDao.findByUsername(user.getUsername()).orElseThrow(() -> new Exception("Usuario no existe"));
	if (u != null && u.getSecret().equals(user.getSecret())) {
	    return u;
	} else
	    throw new Exception("Contraseña incorrecta");
    }

    public User findByUsername(String username) throws Exception {
	User p = usuarioDao.findByUsername(username).orElseThrow(() -> new Exception("Usuario no existe"));
	if (p != null) {
	    return p;
	} else
	    throw new Exception("Usuario no existe");
    }

    public User findById(Long id) throws Exception {
	User p = usuarioDao.findById(id).orElseThrow(() -> new Exception("Usuario no existe"));
	if (p != null) {
	    return p;
	} else
	    throw new Exception("Usuario no existe");
    }

}
