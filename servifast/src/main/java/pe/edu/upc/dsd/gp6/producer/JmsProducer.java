package pe.edu.upc.dsd.gp6.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Component
public class JmsProducer {

    @Autowired
    JmsTemplate jmsTemplate;

    @Value("${jms.servifast.name}")
    String nombreCola;

    public void send(String mensaje) {
	jmsTemplate.convertAndSend(nombreCola, mensaje);
    }

}
