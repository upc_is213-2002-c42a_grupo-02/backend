package pe.edu.upc.dsd.gp6.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "users")
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "username", length = 50, unique = true, nullable = false)
    private String username;

    @Column(name = "email", unique = true, nullable = false)
    private String email;

    @Column(name = "secret", nullable = false)
    private String secret;

    @Column(name = "name", length = 50, nullable = false)
    private String name;

    @Column(name = "lastname", length = 50)
    private String lastname;

    @Column(name = "date_birth", columnDefinition = "DATE")
    private Date datebirth;

    @Embedded
    @JsonProperty("location")
    private UserAddress address;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @Transient
    @JsonIgnore
    private List<Request> requests;

    @Override
    public String toString() {
	return "User [id=" + id + ", username=" + username + ", email=" + email + ", secret=" + secret + ", name="
		+ name + ", lastname=" + lastname + ", datebirth=" + datebirth + ", address=" + address + "]";
    }

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getUsername() {
	return username;
    }

    public void setUsername(String username) {
	this.username = username;
    }

    public String getEmail() {
	return email;
    }

    public void setEmail(String email) {
	this.email = email;
    }

    public String getSecret() {
	return secret;
    }

    public void setSecret(String secret) {
	this.secret = secret;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public String getLastname() {
	return lastname;
    }

    public void setLastname(String lastname) {
	this.lastname = lastname;
    }

    public Date getDatebirth() {
	return datebirth;
    }

    public void setDatebirth(Date datebirth) {
	this.datebirth = datebirth;
    }

    public UserAddress getAddress() {
	return address;
    }

    public void setAddress(UserAddress address) {
	this.address = address;
    }

}
