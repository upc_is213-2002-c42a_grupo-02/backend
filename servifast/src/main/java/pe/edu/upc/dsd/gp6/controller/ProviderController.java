package pe.edu.upc.dsd.gp6.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import pe.edu.upc.dsd.gp6.model.Provider;
import pe.edu.upc.dsd.gp6.service.ProviderService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/")
public class ProviderController {

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    ProviderService providerService;

    @PostMapping("/provider")
    public Provider registrar(@RequestBody Provider provider) {
	Provider p;
	try {
	    p = providerService.registrar(provider);
	} catch (Exception e) {
	    throw new ResponseStatusException(HttpStatus.NOT_FOUND, "El proveedor no se pudo crear", e);
	}
	return p;
    }

    @PutMapping("/provider")
    public Provider actualizar(@RequestBody Provider provider) {
	Provider p;
	try {
	    p = providerService.registrar(provider);
	} catch (Exception e) {
	    throw new ResponseStatusException(HttpStatus.NOT_FOUND, "El proveedor no se pudo crear", e);
	}
	return p;
    }

    @GetMapping("/providers")
    public List<Provider> list() {
	List<Provider> p;
	try {
	    p = restTemplate.getForObject("http://resource-service/providers", List.class);
	} catch (Exception e) {
	    throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No existe informacion de proveedores", e);
	}
	return p;
    }

    @PostMapping("/provider/login")
    public Provider login(@RequestBody Provider provider) {
	Provider p;
	try {
	    p = providerService.login(provider);
	} catch (Exception e) {
	    throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Error de autentificación", e);
	}
	return p;
    }

    @GetMapping("/provider/{id}")
    public Provider login(@PathVariable("id") Long id) {
	Provider p;
	try {
	    Map<String, String> uriParams = new HashMap<String, String>();
	    uriParams.put("id", id.toString());
	    p = restTemplate.getForObject("http://resource-service/provider/{id}", Provider.class, uriParams);
	} catch (Exception e) {
	    throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Error al obtener proveedor", e);
	}
	return p;
    }

    @GetMapping("/provider/find/{username}")
    public Provider login(@PathVariable("username") String username) {
	Provider p;
	try {
	    Map<String, String> uriParams = new HashMap<String, String>();
	    uriParams.put("username", username);
	    p = restTemplate.getForObject("http://resource-service/provider/find/{username}", Provider.class,
		    uriParams);
	} catch (Exception e) {
	    throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Error al obtener proveedor", e);
	}
	return p;
    }

}
