package pe.edu.upc.dsd.gp6.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import pe.edu.upc.dsd.gp6.model.Provider;

public interface ProviderDao extends CrudRepository<Provider, Long> {

    @Query(value = "select p from Provider p where p.username=:username")
    public Optional<Provider> findByUsername(@Param("username") String username);

}
