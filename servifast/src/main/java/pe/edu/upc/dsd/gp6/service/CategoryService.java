package pe.edu.upc.dsd.gp6.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.edu.upc.dsd.gp6.dao.CategoryDao;
import pe.edu.upc.dsd.gp6.model.Category;

@Service
public class CategoryService {

    @Autowired
    CategoryDao categoryDao;

    @Transactional
    public Category registrar(Category category) {
	return categoryDao.save(category);
    }

    public List<Category> list() {
	return (List<Category>) categoryDao.findAll();
    }

}
