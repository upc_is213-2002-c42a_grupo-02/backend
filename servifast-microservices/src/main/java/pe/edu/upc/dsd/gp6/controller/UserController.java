package pe.edu.upc.dsd.gp6.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import pe.edu.upc.dsd.gp6.model.User;
import pe.edu.upc.dsd.gp6.service.UserService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/")
public class UserController {

    @Autowired
    UserService usuarioService;

    @GetMapping("/users")
    public List<User> list() {
	List<User> p;
	try {
	    p = usuarioService.list();
	} catch (Exception e) {
	    throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No se pudo obtener información de usuarios", e);
	}
	return p;
    }

    @GetMapping("/user/{id}")
    public User login(@PathVariable("id") Long id) {
	User p;
	try {
	    p = usuarioService.findById(id);
	} catch (Exception e) {
	    throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Error al obtener usuario", e);
	}
	return p;
    }

    @GetMapping("/user/find/{username}")
    public User login(@PathVariable("username") String username) {
	User p;
	try {
	    p = usuarioService.findByUsername(username);
	} catch (Exception e) {
	    throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Error al obtener usuario", e);
	}
	return p;
    }

}
