package pe.edu.upc.dsd.gp6.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ubigeo")
public class Ubigeo implements Serializable {

    @Id
    private Integer id;

    @Column
    private String departament;

    @Column
    private String province;

    @Column
    private String district;

    @Override
    public String toString() {
	return "Ubigeo [id=" + id + ", departament=" + departament + ", province=" + province + ", district=" + district
		+ "]";
    }

    public Integer getId() {
	return id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public String getDepartament() {
	return departament;
    }

    public void setDepartament(String departament) {
	this.departament = departament;
    }

    public String getProvince() {
	return province;
    }

    public void setProvince(String province) {
	this.province = province;
    }

    public String getDistrict() {
	return district;
    }

    public void setDistrict(String district) {
	this.district = district;
    }

}
