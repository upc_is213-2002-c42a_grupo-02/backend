package pe.edu.upc.dsd.gp6.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.edu.upc.dsd.gp6.dao.ProviderDao;
import pe.edu.upc.dsd.gp6.model.Provider;

@Service
public class ProviderService {

    @Autowired
    ProviderDao providerDao;

    public List<Provider> list() {
	return (List<Provider>) providerDao.findAll();
    }

    public Provider login(Provider provider) throws Exception {
	Provider p = providerDao.findByUsername(provider.getUsername())
		.orElseThrow(() -> new Exception("Proveedor no existe"));
	if (p != null && p.getSecret().equals(provider.getSecret())) {
	    return p;
	} else
	    throw new Exception("Contraseña incorrecta");
    }

    public Provider findByUsername(String username) throws Exception {
	Provider p = providerDao.findByUsername(username).orElseThrow(() -> new Exception("Proveedor no existe"));
	if (p != null) {
	    return p;
	} else
	    throw new Exception("Proveedor no existe");
    }

    public Provider findById(Long id) throws Exception {
	Provider p = providerDao.findById(id).orElseThrow(() -> new Exception("Proveedor no existe"));
	if (p != null) {
	    return p;
	} else
	    throw new Exception("Proveedor no existe");
    }

}
