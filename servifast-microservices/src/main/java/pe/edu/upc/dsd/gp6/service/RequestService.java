package pe.edu.upc.dsd.gp6.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.edu.upc.dsd.gp6.dao.ProviderDao;
import pe.edu.upc.dsd.gp6.dao.RequestDao;
import pe.edu.upc.dsd.gp6.dao.UserDao;
import pe.edu.upc.dsd.gp6.model.Request;

@Service
public class RequestService {

    @Autowired
    RequestDao requestDao;

    @Autowired
    UserDao userDao;

    @Autowired
    ProviderDao providerDao;


    public List<Request> findByUser(String username) {
	return requestDao.findByUser(username);
    }

    public List<Request> findByProvider(String username) {
	return requestDao.findByProvider(username);
    }

}
