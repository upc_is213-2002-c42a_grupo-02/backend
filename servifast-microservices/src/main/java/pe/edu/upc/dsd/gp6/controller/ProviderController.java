package pe.edu.upc.dsd.gp6.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import pe.edu.upc.dsd.gp6.model.Provider;
import pe.edu.upc.dsd.gp6.service.ProviderService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/")
public class ProviderController {

    @Autowired
    ProviderService providerService;

    @GetMapping("/providers")
    public List<Provider> list() {
	List<Provider> p;
	try {
	    p = providerService.list();
	} catch (Exception e) {
	    throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No existe informacion de proveedores", e);
	}
	return p;
    }

    @GetMapping("/provider/{id}")
    public Provider login(@PathVariable("id") Long id) {
	Provider p;
	try {
	    p = providerService.findById(id);
	} catch (Exception e) {
	    throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Error al obtener proveedor", e);
	}
	return p;
    }

    @GetMapping("/provider/find/{username}")
    public Provider login(@PathVariable("username") String username) {
	Provider p;
	try {
	    p = providerService.findByUsername(username);
	} catch (Exception e) {
	    throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Error al obtener proveedor", e);
	}
	return p;
    }

}
