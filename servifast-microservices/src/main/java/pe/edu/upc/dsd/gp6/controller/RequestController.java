package pe.edu.upc.dsd.gp6.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import pe.edu.upc.dsd.gp6.model.Request;
import pe.edu.upc.dsd.gp6.service.RequestService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/")
public class RequestController {

    @Autowired
    RequestService requestService;

    @GetMapping("/requests/user/{username}")
    public List<Request> lisBytUser(@PathVariable("username") String username) {
	List<Request> p;
	try {
	    p = requestService.findByUser(username);
	} catch (Exception e) {
	    throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
	}
	return p;
    }

    @GetMapping("/requests/provider/{username}")
    public List<Request> listByProvider(@PathVariable("username") String username) {
	List<Request> p;
	try {
	    p = requestService.findByProvider(username);
	} catch (Exception e) {
	    throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
	}
	return p;
    }

}
