package pe.edu.upc.dsd.gp6.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import pe.edu.upc.dsd.gp6.model.Request;

public interface RequestDao extends CrudRepository<Request, Long> {

    @Query("select p from Request p where p.user.username=:username")
    public List<Request> findByUser(@Param("username") String username);

    @Query("select p from Request p where p.provider.username=:username")
    public List<Request> findByProvider(@Param("username") String username);

}
