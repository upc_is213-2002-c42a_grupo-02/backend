package pe.edu.upc.dsd.gp6.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import pe.edu.upc.dsd.gp6.model.Category;
import pe.edu.upc.dsd.gp6.service.CategoryService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/")
public class CategoryController {

    @Autowired
    CategoryService categoryService;

    @GetMapping("/categories")
    public List<Category> list() {
	List<Category> p;
	try {
	    p = categoryService.list();
	} catch (Exception e) {
	    throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No existe informacion de proveedores", e);
	}
	return p;
    }

}
