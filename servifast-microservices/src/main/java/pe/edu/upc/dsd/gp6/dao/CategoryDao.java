package pe.edu.upc.dsd.gp6.dao;

import org.springframework.data.repository.CrudRepository;

import pe.edu.upc.dsd.gp6.model.Category;

public interface CategoryDao extends CrudRepository<Category, Long> {

}
