package pe.edu.upc.dsd.gp6.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import pe.edu.upc.dsd.gp6.model.User;

public interface UserDao extends CrudRepository<User, Long> {

    @Query(value = "select p from User p where p.username=:username")
    public Optional<User> findByUsername(@Param("username") String username);

}
