package pe.edu.upc.dsd.gp6;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class ServifastEurekaApplication {

    public static void main(String[] args) {
	SpringApplication.run(ServifastEurekaApplication.class, args);
    }

}
